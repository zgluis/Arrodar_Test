package com.zgluis.arrodar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class Begin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Start();
    }

    private void Start() {
        SharedPreferences session;
        session = getSharedPreferences("session", Context.MODE_PRIVATE);
        String value = session.getString("email", "");
        Log.d("arrodar_begin", "Sesion:" + value);
        if (!value.isEmpty()) {
            Intent intent = new Intent(getApplicationContext(), Home_main.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }else{
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);        }
    }
}
