package com.zgluis.arrodar.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.zgluis.arrodar.R;
import com.zgluis.arrodar.app.AppController;
import com.zgluis.arrodar.model.Viaje;

import java.util.List;


public class Misviajes_adapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Viaje> viajeItems;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();


    public Misviajes_adapter(Activity activity, List<Viaje> viajeItems) {
        this.activity = activity;
        this.viajeItems = viajeItems;
    }

    @Override
    public int getCount() {
        return viajeItems.size();
    }

    @Override
    public Object getItem(int location) {
        return viajeItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_row_misviajes, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.thumbnail);
        TextView nombre = (TextView) convertView.findViewById(R.id.nombre);
        TextView apellido = (TextView) convertView.findViewById(R.id.apellido);
        TextView salida = (TextView) convertView.findViewById(R.id.salida);
        TextView llegada = (TextView) convertView.findViewById(R.id.llegada);
        TextView fecha = (TextView) convertView.findViewById(R.id.fecha);
        TextView hora = (TextView) convertView.findViewById(R.id.hora);
        TextView reputacion = (TextView) convertView.findViewById(R.id.reputacion);
        TextView telefono = (TextView) convertView.findViewById(R.id.telefono);
        TextView correo = (TextView) convertView.findViewById(R.id.correo);

        TextView pasajeros = (TextView) convertView.findViewById(R.id.pasajeros);

        TextView vehiculo = (TextView) convertView.findViewById(R.id.vehiculo);
        TextView placa = (TextView) convertView.findViewById(R.id.placa);

        TextView micalificacion = (TextView) convertView.findViewById(R.id.micalificacion);

        // getting viaje data for the row
        Viaje m = viajeItems.get(position);

        // thumbnail image
        thumbNail.setImageUrl(m.getThumbnailUrl(), imageLoader);

        // nombre
        nombre.setText(m.getNombre());

        // apellido
        apellido.setText(m.getApellido());

        // salida
        salida.setText(activity.getString(R.string.salida, String.valueOf(m.getSalida())));

        // llegada
        llegada.setText(activity.getString(R.string.llegada, String.valueOf(m.getLlegada())));

        // fecha
        fecha.setText(String.valueOf(m.getFecha()));

        // llegada
        hora.setText(String.valueOf(m.getHora()));

        // reputacion
        reputacion.setText(String.valueOf(m.getReputacion()));

        // telefono
        telefono.setText(activity.getString(R.string.telefono, String.valueOf(m.getTelefono())));

        // correo
        correo.setText(activity.getString(R.string.correo, String.valueOf(m.getCorreo())));

        //Lista de pasajeros
        pasajeros.setText(activity.getString(R.string.pasajeros, String.valueOf(m.getPasajeros())));

        //vehiculo("marca, modelo")
        vehiculo.setText(activity.getString(R.string.vehiculo, String.valueOf(m.getMarca()),String.valueOf(m.getModelo())));

        //placa
        placa.setText(activity.getString(R.string.placa, String.valueOf(m.getPlaca())));

        micalificacion.setText(activity.getString(R.string.micalificacion, String.valueOf(m.getMicalificacion())));

        return convertView;
    }

}
