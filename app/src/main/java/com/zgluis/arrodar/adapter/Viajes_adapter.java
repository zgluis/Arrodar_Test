package com.zgluis.arrodar.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.zgluis.arrodar.R;
import com.zgluis.arrodar.app.AppController;
import com.zgluis.arrodar.model.Viaje;

import java.util.List;

public class Viajes_adapter extends BaseAdapter {
    private Activity activity;
    private LayoutInflater inflater;
    private List<Viaje> viajeItems;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();


    public Viajes_adapter(Activity activity, List<Viaje> viajeItems) {
        this.activity = activity;
        this.viajeItems = viajeItems;
    }


    @Override
    public int getCount() {
        return viajeItems.size();
    }

    @Override
    public Object getItem(int location) {
        return viajeItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
             convertView = inflater.inflate(R.layout.list_row, null);
        }

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.thumbnail);
        TextView nombre = (TextView) convertView.findViewById(R.id.nombre);
        TextView apellido = (TextView) convertView.findViewById(R.id.apellido);
        TextView salida = (TextView) convertView.findViewById(R.id.salida);
        TextView llegada = (TextView) convertView.findViewById(R.id.llegada);
        TextView fecha = (TextView) convertView.findViewById(R.id.fecha);
        TextView hora = (TextView) convertView.findViewById(R.id.hora);
        TextView reputacion = (TextView) convertView.findViewById(R.id.reputacion);

        // getting movie data for the row
        Viaje m = viajeItems.get(position);

        // thumbnail image
        thumbNail.setImageUrl(m.getThumbnailUrl(), imageLoader);

        // nombre
        nombre.setText(m.getNombre());

        // apellido
        apellido.setText(m.getApellido());

        // salida
        salida.setText(activity.getString(R.string.salida, String.valueOf(m.getSalida())));

        // llegada
        llegada.setText(activity.getString(R.string.llegada, String.valueOf(m.getLlegada())));

        // fecha
        fecha.setText(String.valueOf(m.getFecha()));

        // llegada
        hora.setText(String.valueOf(m.getHora()));

        // reputacion
        reputacion.setText(String.valueOf(m.getReputacion()));

        return convertView;
    }


}
