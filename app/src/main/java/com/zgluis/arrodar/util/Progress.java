package com.zgluis.arrodar.util;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.app.ProgressDialog;


public class Progress extends Application {
    /*
    layoutV= contexto del layout donde se va a mostrar
    progressV= My progress View;
    */

    /**
     * Shows the progress UI and hides the form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static void showProgressLayout(final boolean show, final View layoutV, final View progressV) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = 200;

            layoutV.setVisibility(show ? View.GONE : View.VISIBLE);
            layoutV.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    layoutV.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressV.setVisibility(show ? View.VISIBLE : View.GONE);
            progressV.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressV.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressV.setVisibility(show ? View.VISIBLE : View.GONE);
            layoutV.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
    public static void showProgressDialog(final boolean show, ProgressDialog pDialog, String Mensaje){
        if (show){
            pDialog.setMessage(Mensaje);
            pDialog.show();
        }else{
            if (pDialog != null) {
                pDialog.dismiss();
                //pDialog = null;
            }
        }
    }





}
