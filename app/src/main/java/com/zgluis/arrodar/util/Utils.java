package com.zgluis.arrodar.util;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Vibrator;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;

import com.zgluis.arrodar.MainActivity;
import com.zgluis.arrodar.R;

import java.io.File;


public class Utils extends Application{

    public static String getValor(EditText et){
        return et.getText().toString().trim();
    }
    public static String getValorSpinner(Spinner sp){
        return sp.getSelectedItem().toString().trim();
    }
    public static String getSwitch(Switch sw){
        if(sw.isChecked()){
            return "1";
        }else{
            return "0";
        }
    }



    public static boolean CheckSession(Context c) {
        SharedPreferences s = c.getSharedPreferences("session", Context.MODE_PRIVATE);
        Log.d("arrodar_home", "Sesion:" + s.getString("email", ""));
        if (s.getString("email", "").isEmpty()) {
            Intent intent = new Intent(c, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            c.startActivity(intent);
            return false;
        }
        return true;
    }

    public static void cerrarSesion(Context c) {
        c.getSharedPreferences("session", 0).edit().clear().commit();
        Utils.CheckSession(c);
    }

    public static String getTag(Context c){
        return c.getClass().getSimpleName();
    }

    public static void  vibrarWarning(Context c){
        int dot = 100;// Length of a Morse Code "dot" in milliseconds
        int short_gap = 50;    // Length of Gap Between dots/dashes
        long[] pattern = {
                0,  // Start immediately
                dot, short_gap, dot
        };
        Vibrator v = (Vibrator) c.getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(pattern,-1);

    }





}
