package com.zgluis.arrodar.util;


import android.app.Application;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class Barrasnack extends AppCompatActivity {



    public static void show(String message,View view) {
        Snackbar snackx = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        view = snackx.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        snackx.show();
    }

    public static void showCustom(String message,View view,int color) {
        Snackbar snackx = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        view = snackx.getView();
        TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(color);
        snackx.show();
    }
}
