package com.zgluis.arrodar.join;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;
import com.zgluis.arrodar.R;
import com.zgluis.arrodar.util.Barrasnack;
import com.zgluis.arrodar.util.Utils;

import org.json.JSONException;
import org.json.JSONObject;


public class Join_dos extends AppCompatActivity {
    EditText et_name,et_lastname,et_ci,et_address,et_telf;
    Switch sw_car;
    JSONObject valores = new JSONObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.join2);

        try {
            valores.put("ci", "0");
            valores.put("nombre", "0");
            valores.put("apellido", "0");
            valores.put("direccion", "0");
            valores.put("zona", "0");
            valores.put("telefono", "0");
            valores.put("id_vehiculo", "0");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Bundle b = getIntent().getExtras();

        if(b!=null){
            String aux = b.getString("valores");
            try {
                valores = new JSONObject(aux);
                Log.d("arrodar_J", "Valores recibidos"+valores.toString());
            } catch (Throwable t) {
                Log.e("arrodar_J", "Could not parse to json Error:"+t);
            }
        }

        et_name = (EditText) findViewById(R.id.et_name);
        et_lastname = (EditText) findViewById(R.id.et_lastname);
        et_ci = (EditText) findViewById(R.id.et_ci);
        et_address = (EditText) findViewById(R.id.et_address);
        et_telf = (EditText) findViewById(R.id.et_telf);
        sw_car = (Switch) findViewById(R.id.sw_car);

    }



    public void join2_next(View view){

        if(TextUtils.isEmpty(Utils.getValor(et_name))||
                TextUtils.isEmpty(Utils.getValor(et_lastname))||
                TextUtils.isEmpty(Utils.getValor(et_ci))||
                TextUtils.isEmpty(Utils.getValor(et_address))||
                TextUtils.isEmpty(Utils.getValor(et_telf)))
        {
            Barrasnack.show("Debe llenar todos los campos", findViewById(android.R.id.content));
        }
        else if(et_ci.getText().toString().trim().length() == 8 || et_ci.getText().toString().trim().length() == 7){
            agregar();
            Intent intent = new Intent(this, Join_tres.class);
            intent.putExtra("valores", valores.toString());
            startActivity(intent);
        }
        else{
            Barrasnack.show("El numero de identificacion no es valido", findViewById(android.R.id.content));
        }

    }



    public void agregar(){
        try {
            valores.put("nombre",Utils.getValor(et_name));
            valores.put("apellido",Utils.getValor(et_lastname));
            valores.put("ci",Utils.getValor(et_ci));
            valores.put("direccion",Utils.getValor(et_address));
            valores.put("telefono",Utils.getValor(et_telf));
            valores.put("id_vehiculo",Utils.getSwitch(sw_car));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
